# Api auth manager plugin #

## Setup ##
Insert following code snippet into OctoberCMS composer.json file:

````
"extra": {
    "laravel": {
        "dont-discover": [
            "tymon/jwt-auth"
        ]
    }
}
````

## Troubleshooting ##
To solve CORS errors, add follow line to the index.php
````
header('Access-Control-Allow-Origin: *');
````