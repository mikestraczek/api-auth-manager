<?php namespace Nextlevels\ApiAuthManager\Http\Controllers;

use Carbon\Carbon;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Mail\Message;
use Illuminate\Routing\Controller;
use Nextlevels\ApiAuthManager\Classes\Exceptions\AuthException;
use Nextlevels\ApiAuthManager\Models\LoginHistory;
use RainLab\User\Models\User;
use Tymon\JWTAuth\Contracts\JWTSubject;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;

/**
 * Class AuthController
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class AuthController extends Controller
{

    /**
     * @var User
     */
    public $user;

    /**
     * @var string
     */
    protected $token;

    /**
     * Login user and return token with user object
     *
     * @param array $credentials
     *
     * @throws AuthException
     * @throws \Tymon\JWTAuth\Exceptions\JWTException
     * @throws \ValidationException
     */
    protected function signIn(array $credentials): void
    {
        \Event::fire('rainlab.user.beforeAuthenticate', [$this, $credentials]);

        if (! $this->token = \JWTAuth::attempt($credentials)) {
            throw new AuthException(\Lang::get('nextlevels.apiauthmanager::lang.api.auth.incorrect_credentials'));
        }

        $this->user = \JWTAuth::setToken($this->token)->authenticate();

        if (! $this->user->is_activated) {
            throw new \ValidationException([
                'activated' => \Lang::get(
                    'nextlevels.apiauthmanager::lang.api.auth.user_not_activated'
                )
            ]);
        }

        if ($this->user->isBanned()) {
            \JWTAuth::parseToken()->invalidate();

            throw new AuthException(\Lang::get('nextlevels.apiauthmanager::lang.api.auth.user_is_banned'));
        }

        if (! $this->user->is_activated) {
            \JWTAuth::parseToken()->invalidate();

            throw new AuthException(\Lang::get('nextlevels.apiauthmanager::lang.api.auth.user_is_not_activated'));
        }

        $this->user->last_login = Carbon::now();

        $this->user->save();

        \Event::fire('rainlab.user.afterAuthenticate', [$this, $credentials]);
    }

    /**
     * Validate token and return the token
     *
     * @return string
     * @throws AuthException
     */
    protected function getToken(): string
    {
        try {
            if (! $token = \JWTAuth::refresh(\JWTAuth::parseToken())) {
                throw new AuthException(\Lang::get('nextlevels.apiauthmanager::lang.api.auth.could_not_refresh_token'));
            }
        } catch (TokenExpiredException $exception) {
            throw new AuthException(\Lang::get('nextlevels.apiauthmanager::lang.api.auth.token_expired'));
        }

        return $token;
    }

    /**
     * Login user return token and user
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws AuthException
     * @throws \Tymon\JWTAuth\Exceptions\JWTException
     * @throws \ValidationException
     */
    public function login(Request $request): JsonResponse
    {
        $rules = ['password' => 'required'];
        $attributeNames = ['password' => \Lang::get('nextlevels.apiauthmanager::lang.fields.password')];
        $validator = \Validator::make($request->all(), $rules, [], $attributeNames);
        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }
        $this->signIn($request->only(['email', 'username', 'password']));

        if (! LoginHistory::where('user_id', $this->user->id)->whereMonth('login_at', Carbon::now()->month)->exists()) {
            LoginHistory::create(['login_at' => Carbon::now(), 'user_id' => $this->user->id]);
        }

        \Event::fire('nextlevels.apiauthmanager.afterLogin', [$this, $request]);

        $token = $this->token;
        $user = $this->user->toArray();

        return \Response::json(compact('token', 'user'));
    }

    /**
     * Register user and return user
     *
     * @param Request $request
     *
     * @return JsonResponse
     * @throws \ValidationException
     */
    public function register(Request $request): JsonResponse
    {
        $data = $request->all();
        $rules = [
            'email'                 => 'required|email',
            'password'              => 'required|confirmed',
            'password_confirmation' => 'required'
        ];
        $attributeNames = [
            'email'                 => \Lang::get('nextlevels.apiauthmanager::lang.fields.email'),
            'password'              => \Lang::get('nextlevels.apiauthmanager::lang.fields.password'),
            'password_confirmation' => \Lang::get('nextlevels.apiauthmanager::lang.fields.password_confirmation')
        ];
        $validator = \Validator::make($data, $rules, [], $attributeNames);

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        \Event::fire('rainlab.user.beforeRegister', [$data]);

        $user = \Auth::register($data);
        $user->activation_code = md5($user->email);
        $user->save();

        \Event::fire('rainlab.user.register', [$user, $data]);

        return \Response::json(compact('user'));
    }

    /**
     * Refresh user token
     *
     * @return JsonResponse
     * @throws AuthException
     */
    public function refreshToken(): JsonResponse
    {
        $token = $this->getToken();
        $user = \JWTAuth::toUser();

        return \Response::json(compact('token', 'user'));
    }

    /**
     * Get current user
     *
     * @return false|JWTSubject|User
     * @throws AuthException
     */
    public function getUser()
    {
        return \JWTAuth::toUser($this->getToken());
    }

    /**
     * Track user activity
     */
    public function trackActivity(): void
    {
        if (($user = $this->getUser()) !== null
            && ! LoginHistory::where('user_id', $user->id)->whereDate('login_at', Carbon::today())->exists()
        ) {
            LoginHistory::create(['login_at' => Carbon::now(), 'user_id' => $user->id]);
        }
    }

    /**
     * Reset password
     *
     * @param Request $request
     *
     * @throws AuthException
     * @throws \ValidationException
     */
    public function resetPassword(Request $request): void
    {
        $data = $request->all();
        $rules = ['email' => 'required|email'];
        $attributeNames = ['email' => \Lang::get('nextlevels.apiauthmanager::lang.fields.email')];
        $validator = \Validator::make($data, $rules, [], $attributeNames);

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        $user = User::findByEmail($data['email']);

        if (! $user || $user->is_guest) {
            throw new AuthException(\Lang::get('rainlab.user::lang.account.invalid_user'));
        }

        $data = [
            'user'     => $user,
            'name'     => $user->name,
            'username' => $user->username,
            'code'     => implode('!', [$user->id, $user->getResetPasswordCode()])
        ];

        \Mail::send('nextlevels.apiauthmanager::mail.restore', $data, function (Message $message) use ($user) {
            $message->to($user->email, $user->full_name);
        });
    }

    /**
     * Restore user password
     *
     * @param Request $request
     *
     * @throws \ValidationException
     */
    public function restorePassword(Request $request): void
    {
        $data = $request->all();
        $rules = [
            'code'     => 'required',
            'password' => 'required|between:' . User::getMinPasswordLength() . ',255'
        ];
        $attributeNames = [
            'code'     => \Lang::get('nextlevels.apiauthmanager::lang.fields.code'),
            'password' => \Lang::get('nextlevels.apiauthmanager::lang.fields.new_password'),
        ];

        $validation = \Validator::make($data, $rules, [], $attributeNames);

        if ($validation->fails()) {
            throw new \ValidationException($validation);
        }

        $errorFields = ['code' => \Lang::get('rainlab.user::lang.account.invalid_activation_code')];
        $parts = explode('!', $data['code']);

        if (\count($parts) !== 2) {
            throw new \ValidationException($errorFields);
        }

        [$userId, $code] = $parts;

        if (! $code || trim($userId) === '' || trim($code) === '') {
            throw new  \ValidationException($errorFields);
        }

        if (! $user = \Auth::findUserById($userId)) {
            throw new  \ValidationException($errorFields);
        }

        if (! $user->attemptResetPassword($code, $data['password'])) {
            throw new  \ValidationException($errorFields);
        }
    }

    /**
     * Change password from current user
     *
     * @param Request $request
     *
     * @throws AuthException
     * @throws \ValidationException
     */
    public function changePassword(Request $request): void
    {
        $data = $request->all();
        $rules = ['password' => 'required|between:' . User::getMinPasswordLength() . ',255'];
        $attributeNames = ['password' => \Lang::get('nextlevels.apiauthmanager::lang.fields.new_password')];

        $validation = \Validator::make($data, $rules, [], $attributeNames);

        if ($validation->fails()) {
            throw new \ValidationException($validation);
        }

        if (($user = $this->getUser()) !== null) {
            $user->setPasswordAttribute(\Hash::make($data['password']));
            $user->save();
        }
    }

    /**
     * Delete given user
     *
     * @param Request $request
     *
     * @throws AuthException
     * @throws \ValidationException
     */
    public function delete(Request $request): void
    {
        $data = $request->all();
        $rules = ['password' => 'required'];
        $attributeNames = ['password' => \Lang::get('nextlevels.apiauthmanager::lang.fields.password')];
        $validator = \Validator::make($data, $rules, [], $attributeNames);

        if ($validator->fails()) {
            throw new \ValidationException($validator);
        }

        \Event::fire('nextlevels.apiauthmanager.beforeDelete', [$this, $request]);

        if (($user = $this->getUser()) === null) {
            throw new \InvalidArgumentException(\Lang::get('nextlevels.apiauthmanager::lang.api.auth.user_not_found'));
        }

        if (! \Hash::check($data['password'], $user->password)) {
            throw new \InvalidArgumentException(
                \Lang::get('nextlevels.apiauthmanager::lang.api.auth.incorrect_password')
            );
        }

        \JWTAuth::unsetToken();

        $user->forceDelete();

        \Event::fire('nextlevels.apiauthmanager.afterDelete', [$this, $request]);
    }
}
