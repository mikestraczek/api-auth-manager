<?php namespace Nextlevels\ApiAuthManager\Http\Middleware;

use Closure;
use Illuminate\Contracts\Routing\ResponseFactory;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class PreflightResponse
 *
 */
class PreflightResponse
{

    /**
     * @param         $request
     * @param Closure $next
     *
     * @return ResponseFactory|mixed|Response
     */
    public function handle($request, Closure $next)
    {
        if ($request->getMethod() === 'OPTIONS') {
            return response('');
        }

        return $next($request);
    }
}
