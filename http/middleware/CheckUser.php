<?php namespace Nextlevels\ApiAuthManager\Http\Middleware;

use Nextlevels\ApiAuthManager\Classes\Exceptions\AuthException;
use Nextlevels\ApiAuthManager\Http\Controllers\AuthController;

/**
 * Class UserRequest
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class CheckUser
{

    /**
     * Check if user logged in
     *
     * @param          $request
     * @param \Closure $next
     *
     * @return mixed
     * @throws AuthException
     * @throws \Tymon\JWTAuth\Exceptions\JWTException
     */
    public function handle($request, \Closure $next)
    {
        if ((new AuthController())->getUser() === null) {
            throw new AuthException(\Lang::get('nextlevels.apiauthmanager::lang.api.auth.user_not_found'));
        }

        return $next($request);
    }
}
