<?php

return [
    'plugin'      => [
        'name'        => 'JWTAuth',
        'description' => 'JSON Web Token Authentication.',
    ],
    'permissions' => [
        'settings' => 'JWTAuth Configuration',
    ],
    'settings'    => [
        'menu_label'       => 'JWTAuth Configuration',
        'menu_description' => 'Change the JWTAuth configuration',
        'secret'           => 'JWT Authentication Secret',
        'secret_comment'   => "Don't forget to set this, as it will be used to sign your tokens."
    ],
    'api' => [
        'auth'        => [
            'incorrect_credentials'   => 'Anmeldedaten sind nicht korrekt',
            'user_is_banned'          => 'Benutzer ist gebannt',
            'user_is_not_activated'   => 'Benutzer ist nicht aktiviert',
            'could_not_refresh_token' => 'Token konnte nicht erneuert werden',
            'token_expired'           => 'Token ist abgelaufen'
        ]
    ]
];
