<?php

use Fruitcake\Cors\HandleCors;
use Nextlevels\ApiAuthManager\Http\Middleware\JsonResponse;

Route::middleware([HandleCors::class, JsonResponse::class])
    ->prefix('api')
    ->namespace('Nextlevels\ApiAuthManager\Http\Controllers')
    ->group(function () {
        Route::post('refresh', 'AuthController@refreshToken');
        Route::post('login', 'AuthController@login');
        Route::post('track-activity', 'AuthController@trackActivity');
        Route::post('register', 'AuthController@register');
        Route::post('reset-password', 'AuthController@resetPassword');
        Route::post('restore-password', 'AuthController@restorePassword');
        Route::post('change-password', 'AuthController@changePassword');
        Route::delete('delete', 'AuthController@delete');
        Route::get('user', 'AuthController@getUser');
    });
