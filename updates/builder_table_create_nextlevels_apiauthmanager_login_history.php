<?php namespace Nextlevels\ApiAuthManager\Updates;

use Schema;
use October\Rain\Database\Updates\Migration;

class BuilderTableCreateNextlevelsApiauthmanagerLoginHistory extends Migration
{
    public function up()
    {
        Schema::create('nextlevels_apiauthmanager_login_history', function($table)
        {
            $table->engine = 'InnoDB';
            $table->increments('id')->unsigned();
            $table->integer('user_id')->nullable();
            $table->dateTime('login_at')->nullable();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('nextlevels_apiauthmanager_login_history');
    }
}
