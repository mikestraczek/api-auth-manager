<?php namespace Nextlevels\ApiAuthManager\Models;

use Backend\Models\User as OctoberUser;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 *
 * @author Slawa Ditzel <slawa.ditzel@next-levels.de>, Next Levels GmbH
 */
class UserBackend extends OctoberUser implements JWTSubject
{

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function isBanned(): array
    {
        return [];
    }
}
