<?php namespace Nextlevels\ApiAuthManager\Models;

use RainLab\User\Models\User as RainLabUser;
use Tymon\JWTAuth\Contracts\JWTSubject;

/**
 * Class User
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class User extends RainLabUser implements JWTSubject
{

    /**
     * Get the identifier that will be stored in the subject claim of the JWT.
     *
     * @return mixed
     */
    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    /**
     * Return a key value array, containing any custom claims to be added to the JWT.
     *
     * @return array
     */
    public function getJWTCustomClaims(): array
    {
        return [];
    }
}
