<?php namespace Nextlevels\ApiAuthManager\Models;

use Model;

/**
 * Class LoginHistory
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class LoginHistory extends Model
{

    /**
     * @var string[]
     */
    protected $dates = ['login_at'];

    /**
     * @var string[]
     */
    protected $fillable = ['user_id', 'login_at'];

    /*
     * Disable timestamps by default.
     * Remove this line if timestamps are defined in the database table.
     */
    public $timestamps = false;

    /**
     * @var string The database table used by the model.
     */
    public $table = 'nextlevels_apiauthmanager_login_history';

    /**
     * @var string[] relations
     */
    public $hasOne = ['user' => \RainLab\User\Models\User::class];
}
