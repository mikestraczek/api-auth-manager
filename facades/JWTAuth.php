<?php namespace Nextlevels\ApiAuthManager\Facades;

use October\Rain\Support\Facade;

/**
 * Class JWTAuth
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class JWTAuth extends Facade
{

    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor(): string
    {
        return 'tymon.jwt.auth';
    }
}
