<?php namespace Nextlevels\ApiAuthManager\Providers;

use Tymon\JWTAuth\Providers\AbstractServiceProvider;

/**
 * Class AuthServiceProvider
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class AuthServiceProvider extends AbstractServiceProvider
{

    /**
     * Boot the service provider.
     */
    public function boot(): void
    {
        if(empty(\Config::get('jwt'))){
            \Config::set('jwt', \Config::get('nextlevels.apiauthmanager::jwt'));
        }
    }
}
