<?php namespace Nextlevels\ApiAuthManager;

use Fruitcake\Cors\CorsServiceProvider;
use Illuminate\Contracts\Http\Kernel;
use Illuminate\Foundation\AliasLoader;
use Nextlevels\ApiAuthManager\Facades\JWTAuth;
use Nextlevels\ApiAuthManager\Http\Middleware\PreflightResponse;
use Nextlevels\ApiAuthManager\Providers\AuthServiceProvider;
use System\Classes\PluginBase;
use Tymon\JWTAuth\Http\Middleware\Authenticate;
use Tymon\JWTAuth\Http\Middleware\RefreshToken;
use Backend\Models\User;
use Backend\Controllers\Users;
/**
 * Class Plugin
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class Plugin extends PluginBase
{

    /**
     * Plugin dependencies.
     *
     * @var array
     */
    public $require = ['RainLab.User'];

    /**
     * Returns information about this plugin.
     *
     * @return array
     */
    public function pluginDetails(): array
    {
        return [
            'name'        => 'nextlevels.apiauthmanager::lang.plugin.name',
            'description' => 'nextlevels.apiauthmanager::lang.plugin.description',
            'author'      => 'Next Levels GmbH',
            'icon'        => 'icon-user-secret',
        ];
    }

    /**
     * @return array
     */
    public function registerPermissions(): array
    {
        return [
            'nextlevels.apiauthmanager.access_settings' => [
                'tab'   => 'nextlevels.apiauthmanager::lang.plugin.name',
                'label' => 'nextlevels.apiauthmanager::lang.permissions.settings'
            ]
        ];
    }

    /**
     * Register method, called when the plugin is first registered.
     */
    public function register(): void
    {
        \App::register(AuthServiceProvider::class);
        \App::register(CorsServiceProvider::class);

        AliasLoader::getInstance()->alias('JWTAuth', JWTAuth::class);

        \App::singleton('auth', function ($app) {
            return new \Illuminate\Auth\AuthManager($app);
        });
    }

    /**
     * Plugin bootstrap
     */
    public function boot(): void
    {
        if (\Request::isMethod('OPTIONS')) {
            $this->app[Kernel::class]->prependMiddleware(PreflightResponse::class);
        }

        $this->app['router']->middleware('jwt.auth', Authenticate::class);
        $this->app['router']->middleware('jwt.refresh', RefreshToken::class);

        \Event::listen('translator.beforeResolve', function ($key, $replaces, $locale) {
            switch ($key) {
                case 'backend::lang.account.login_placeholder':
                    return 'email';
                case 'backend::lang.user.menu_label':
                    return 'Users';
                case 'backend::lang.user.menu_description':
                    return 'Manage all users';
            }
        });

        // Use the email address to process logins
        User::$loginAttribute = 'email';

        User::extend(function($model) {
            // Remove the login rules. The email will be used so no additional
            // validation is needed here.
            $model->rules = array_merge($model->rules, [
                'login' => '',
            ]);

            // Copy email over to the login attribute
            $model->bindEvent('model.beforeSave', function () use ($model) {
                $model->login = $model->email;
            });
        });

        // Remove the login attribute from backend forms and use only the email attribute
        \Event::listen('backend.form.extendFieldsBefore', function ($form) {
            if (!$form->model instanceof User || !$form->getController() instanceof Users) {
                return;
            }
            $fields = $form->tabs['fields'];

            // Remove the login field and make email full width
            if (isset($fields['email']) && is_array($fields['email'])) {
                unset($fields['login']);
                $fields['email'] = array_merge($fields['email'], [
                    'span' => 'full',
                ]);
                $form->tabs = array_merge($form->tabs, ['fields' => $fields]);
            }
        });

        // Remove the login column from the backend list
        Users::extendListColumns(function ($list, $model) {
            $list->removeColumn('login');
        });

    }

    /**
     * @return array|string[]
     */
    public function registerMailTemplates():array
    {
        return ['nextlevels.apiauthmanager::mail.restore'];
    }
}
