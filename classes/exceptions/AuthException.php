<?php namespace Nextlevels\ApiAuthManager\Classes\Exceptions;

/**
 * Class AuthException
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class AuthException extends \Exception
{

    // Empty
}
