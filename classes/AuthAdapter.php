<?php namespace Nextlevels\ApiAuthManager\Classes;

use October\Rain\Auth\AuthException;
use Tymon\JWTAuth\Contracts\Providers\Auth as AuthInterface;

/**
 * Class AuthAdapter
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class AuthAdapter implements AuthInterface
{

    /**
     * @var AuthManager
     */
    protected $auth;

    /**
     * AuthAdapter constructor.
     */
    public function __construct()
    {
        $this->auth = AuthManager::instance();
    }

    /**
     * Get user by given credentials
     *
     * @param array $credentials
     *
     * @return mixed
     */
    public function byCredentials(array $credentials = [])
    {
        try {
            $user = $this->auth->findUserByCredentials($credentials);
            $this->auth->setUser($user);

            return $user;
        } catch (AuthException $e) {
            return false;
        }
    }

    /**
     * Get user by given ID
     *
     * @param mixed $id
     *
     * @return mixed
     */
    public function byId($id)
    {
        if (($user = $this->auth->findUserById($id)) !== null) {
            $this->auth->setUser($user);
        }

        return $user;
    }

    /**
     * Get the currently authenticated user
     *
     * @return mixed
     */
    public function user()
    {
        return $this->auth->getUser();
    }

    /**
     * Register user
     *
     * @param array   $data
     * @param bool $activate
     *
     * @return mixed
     */
    public function register($data, $activate = false)
    {
        return $this->auth->register($data, $activate);
    }
}
