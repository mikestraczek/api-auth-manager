<?php namespace Nextlevels\ApiAuthManager\Classes;

use Nextlevels\ApiAuthManager\Models\UserBackend;
use Backend\Classes\AuthManager as OctoberAuthManager;

/**
 * Class AuthManager
 *
 * @author Slawa Ditzel <slawa.ditzel@next-levels.de>, Next Levels GmbH
 */
class AuthManagerBackend extends OctoberAuthManager
{

    /**
     * @var string
     */
    protected $userModel = UserBackend::class;
}
