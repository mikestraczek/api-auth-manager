<?php namespace Nextlevels\ApiAuthManager\Classes;

use Nextlevels\ApiAuthManager\Models\User;
use RainLab\User\Classes\AuthManager as RainLabAuthManager;

/**
 * Class AuthManager
 *
 * @author Mike Straczek <mike.straczek@next-levels.de>, Next Levels GmbH
 */
class AuthManager extends RainLabAuthManager
{

    /**
     * @var string
     */
    protected $userModel = User::class;
}
